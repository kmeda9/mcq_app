import {produce} from 'immer';
import questions from './questions.json';
export default function(state = initialState, action) {
  return produce(state, (draft) => {
    
    switch (action.type) {
      case 'NEXT_QUESTION':
        draft.position++
        return;
      case 'PREV_QUESTION':
        draft.position--
        return;
      case 'ANSWER_QUESTION':
        draft.questions[`${action.position - 1}`].response = action.value
        return;
      case 'SUBMIT_ANSWERS':
        draft.submitted = true;
        return;
        case 'NUKE_STATE':
        return initialState
      default:
        return
    }
  })
}

const initialState = {
  questions: JSON.parse(questions),
  position: '1',
  submitted: false
};

