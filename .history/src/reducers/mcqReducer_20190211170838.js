import {produce} from 'immer';

export default function(state = initialState, action) {
  return produce(state, (draft) => {
    
    switch (action.type) {
      case 'ANSWER':
        return;
      default:
        return
    }
  })
}

const initialState = {
  questions: [
    {
      question: 'Random Q ?',
      options: {a: '', b: '', c: '', d: ''},
      answer: 'a',
      response: ''
    }
  ],
  position: '',
  submitted: false
};
