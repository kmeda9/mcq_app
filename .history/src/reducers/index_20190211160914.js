import { combineReducers } from 'redux';
import mcqReducer from './mcqReducer';

export default combineReducers({
  mcq: mcqReducer
});
