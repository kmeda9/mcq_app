import {produce} from 'immer';

export default function(state = initialState, action) {
  return produce(state, (draft) => {
    
    switch (action.type) {
      case 'NEXT_QUESTION':
        draft.position++
        return;
      case 'PREV_QUESTION':
        draft.position--
        return;
      default:
        return
    }
  })
}

const initialState = {
  questions: [
    {
      question: 'Random Q ?',
      options: {a: 'A', b: 'B', c: 'C', d: 'D'},
      answer: 'a',
      response: ''
    },
    {
      question: 'Random Q ?',
      options: {a: '', b: '', c: '', d: ''},
      answer: 'a',
      response: ''
    },
    {
      question: 'Random Q ?',
      options: {a: '', b: '', c: '', d: ''},
      answer: 'a',
      response: ''
    },
    {
      question: 'Random Q ?',
      options: {a: '', b: '', c: '', d: ''},
      answer: 'a',
      response: ''
    }
  ],
  position: '1',
  submitted: false
};
