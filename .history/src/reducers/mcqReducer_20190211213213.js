import {produce} from 'immer';

export default function(state = initialState, action) {
  return produce(state, (draft) => {
    
    switch (action.type) {
      case 'NEXT_QUESTION':
        draft.position++
        return;
      case 'PREV_QUESTION':
        draft.position--
        return;
      case 'ANSWER_QUESTION':
        draft.questions[`${action.position - 1}`].response = action.value
        return;
      case 'SUBMIT_ANSWERS':
        draft.submitted = true;
        return;
        case 'NUKE_STATE':
        draft = initialState
        return
      default:
        return
    }
  })
}

const initialState = {
  questions: [
    {
      question: 'Random Q ?',
      options: {'1': 'A', '2': 'B', '3': 'C', '4': 'D'},
      answer: '2',
      response: null
    },
    {
      question: 'Random Q ?',
      options: {'1': '', '2': '', '3': '', '4': ''},
      answer: '3',
      response: null
    },
    {
      question: 'Random Q ?',
      options: {'1': '', '2': '', '3': '', '4': ''},
      answer: 'a',
      response: null
    },
    {
      question: 'Random Q ?',
      options: {'1': '', '2': '', '3': '', '4': ''},
      answer: 'a',
      response: null
    }
  ],
  position: '1',
  submitted: false
};
