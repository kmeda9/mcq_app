import {produce} from 'immer';

export default function(state = initialState, action) {
  return produce(state, (draft) => {
    
    switch (action.type) {
      case 'NEXT_QUESTION':
        draft.position++
        return;
      case 'PREV_QUESTION':
        draft.position--
        return;
      case 'ANSWER_QUESTION':
        draft.questions[`${action.position - 1}`].response = action.value
        return;
      
      default:
        return
    }
  })
}

const initialState = {
  questions: [
    {
      question: 'Random Q ?',
      options: {'1': 'A', '2': 'B', '3': 'C', '4': 'D'},
      answer: '2',
      response: ''
    },
    {
      question: 'Random Q ?',
      options: {'1': '', '2': '', '3': '', '4': ''},
      answer: '3',
      response: ''
    },
    {
      question: 'Random Q ?',
      options: {'1': '', '2': '', '3': '', '4': ''},
      answer: 'a',
      response: ''
    },
    {
      question: 'Random Q ?',
      options: {'1': '', '2': '', '3': '', '4': ''},
      answer: 'a',
      response: ''
    }
  ],
  position: '1',
  submitted: false
};
