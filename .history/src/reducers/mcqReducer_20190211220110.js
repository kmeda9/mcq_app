import {produce} from 'immer';

export default function(state = initialState, action) {
  return produce(state, (draft) => {
    
    switch (action.type) {
      case 'NEXT_QUESTION':
        draft.position++
        return;
      case 'PREV_QUESTION':
        draft.position--
        return;
      case 'ANSWER_QUESTION':
        draft.questions[`${action.position - 1}`].response = action.value
        return;
      case 'SUBMIT_ANSWERS':
        draft.submitted = true;
        return;
        case 'NUKE_STATE':
        return initialState
      default:
        return
    }
  })
}

const initialState = {
  "questions": [
    {
    "question": "What is the scientific name of a butterfly?",
    "options": {
    "1": "Apis",
    "2": "Coleoptera",
    "3": "Formicidae",
    "4": "Rhopalocera"},
    "answer": '4',
    "response": null
    },
    {
    "question": "How hot is the surface of the sun?",
    "options": 
    {
    "1": "1,233 K",
    "2": "5,778 K",
    "3": "12,130 K",
    "4": "101,300 K"
  }
    ,
    "answer": '2'
    },
    {
    "question": "Who are the actors in The Internship?",
    "options": 
    {"1": "Ben Stiller, Jonah Hill",
    "2": "Courteney Cox, Matt LeBlanc",
    "3": "Kaley Cuoco, Jim Parsons",
    "4": "Vince Vaughn, Owen Wilson"}
    ,
    "answer": '4'
    },
    {
    "question": "What is the capital of Spain?",
    "options": 
    {
    "1":"Berlin",
    "2":"Buenos Aires",
    "3":"Madrid",
    "4": "San Juan"},
    "answer": '3',
    "response": null
    },
    {
    "question": "What are the school colors of the University of Texas at Austin?",
    "options": {
    "1":"Black, Red",
    "2":"Blue, Orange",
    "3":"White, Burnt Orange",
    "4":"White, Old gold, Gold"
    },
    "answer": '3',
    "response": null
    },
    {
    "question": "What is 70 degrees Fahrenheit in Celsius?",
    "options": {
    "1":"18.8889",
    "2":"20",
    "3":"21.1111",
    "4":"158"}
    ,
    "answer": '3',
    "response": null
    },
    {
    "question": "When was Mahatma Gandhi born?",
    "options": {
    "1":"October 2, 1869",
    "2":"December 15, 1872",
    "3":"July 18, 1918",
    "4":"January 15, 1929"
    },
    "answer": '1',
    "response": null
    },
    {
    "question": "How far is the moon from Earth?",
    "options": {
    "1":"7,918 miles (12,742 km)",
    "2":"86,881 miles (139,822 km)",
    "3":"238,400 miles (384,400 km)",
    "4":"35,980,000 miles (57,910,000 km)"
    },
    "answer": '3',
    "response": null
    },
    {
    "question": "What is 65 times 52?",
    "options": 
    {"1":"117",
    "2":"3120",
    "3":"3380",
    "4":"3520"
    },
    "answer": '3',
    "response": null
    },
    {
    "question": "How tall is Mount Everest?",
    "options": {
    "1":"6,683 ft (2,037 m)",
    "2":"7,918 ft (2,413 m)",
    "3":"19,341 ft (5,895 m)",
    "4":"29,029 ft (8,847 m)"}
    ,
    "answer": '4',
    "response": null
    },
    {
    "question": "When did The Avengers come out?",
    "options": {
    "1": "May 2, 2008",
    "2": "May 4, 2012",
    "3":"May 3, 2013",
    "4": "April 4, 2014"
    },
    "answer": '2',
    "response": null
  
    },
    {
    "question": "What is 48,879 in hexidecimal?",
    "options": 
    {
      "1": "0x18C1",
    "2": "0xBEEF",
    "3": "0xDEAD",
    "4": "0x12D591"}
    ,
    "answer": '2',
    "response": null
    }
    ],
  position: '1',
  submitted: false
};

