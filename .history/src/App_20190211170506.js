import React, { Component, Fragment } from 'react';
import './App.sass';
import Header from './components/Header';
import Body from './components/Body';
import Controls from './components/Controls';

class App extends Component {
  render() {
    return (
      <Fragment>
        <div className="wrapper">
          <div className="mcq-container">

            <Header text="Quiz App" />
            <Body/>
            <Controls/>
            
          </div>
        </div>
      </Fragment>
    );
  }
}

export default App;
