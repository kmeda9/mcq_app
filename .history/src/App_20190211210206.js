import React, {Component, Fragment} from 'react';
import './App.sass';
import Header from './components/Header';
import Body from './components/Body';
import Controls from './components/Controls';

import {connect} from 'react-redux';
import mcqReducer from './reducers/mcqReducer';

const mapStateToProps = state => {
  return {submitted: mcqReducer.submitted}
}

class App extends Component {
  render() {
    return (
      <Fragment>
        <div className="wrapper">
          <div className="mcq-container">

            <Header text="Quiz App"/> {!this.props.submitted
              ? <Fragment>
                  <Body/>
                  <Controls/>
                </Fragment>
              : <div>
                <button className="mcq-btn mcq-results">Results</button>
              </div>
}

          </div>
        </div>
      </Fragment>
    );
  }
}

export default connect()(App);
