import React, {Component, Fragment} from 'react';
import './App.sass';
import Header from './components/Header';
import Body from './components/Body';
import Controls from './components/Controls';

import {connect} from 'react-redux';
import mcqReducer from './reducers/mcqReducer';

const mapStateToProps = state => {
  return {
    submitted: state.mcq.submitted,
    questions: state.mcq.questions
  }
}

class App extends Component {
  constructor(){
    super();
    this.state={displayResults: false }
    this.showResults = this.showResults.bind(this);
  }
  
  showResults(){
    this.setState(prevState => {
      return {
        displayResults: !prevState.displayResults
      }
    }, () => alert(this.state.displayResults));
    
  }

  renderResults(){
    const {questions} = this.props;
    return (
      questions.map((question, i) => {
        return (
          <div key = {i}>
            <i>{`${i+1})`}</i>
            <span>{question}</span>
            <div>Answer: {question.answer} </div>
            <div>Given: {question.response} </div>
          </div>
        )
      })
    )
  }
  
  render() {
    return (
      <Fragment>
        <div className="wrapper">
          <div className="mcq-container">

            <Header text="Quiz App"/> 
            {this.props.submitted
              ? <Fragment>
                  <Body/>
                  <Controls/>
                </Fragment>
              : 
              <Fragment>
                <div style={{
                  width: '100%',
                  height: '80%',
                  padding: '20px',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexDirection: 'column',
                }}> 
                  <div>Your's answers have been submitted!</div>
                  <div>Click on Results to verify!</div>
                <div style={{marginTop: 20}}>
                  <button 
                    className="mcq-btn mcq-results"
                    onClick={this.showResults}
                    >Results</button>
                </div>
                </div>
              </Fragment>
}

          </div>
        </div>
      </Fragment>
    );
  }
}

export default connect(mapStateToProps)(App);
