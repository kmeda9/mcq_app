import React, { Component, Fragment } from 'react';
import './App.sass';
import Header from './components/Header';
import Body from './components/Body';
import Controls from './components/Controls';

class App extends Component {
  render() {
    return (
      <Fragment>
        <div className="wrapper">
          <div className="mcq-container">

            <Header 
              text="Quiz App"
              />
            
            <Body/>

          
            <Controls/>
          <div className="mcq-prog">
            <div className="mcq-remaining">1 / 10</div>
            <div className="mcq-prog-bar"></div>
          </div>

          </div>
        </div>
      </Fragment>
    );
  }
}

export default App;
