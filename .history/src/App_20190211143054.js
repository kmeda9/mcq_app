import React, { Component } from 'react';

class App extends Component {
  render() {
    return (
      <div className="App">
        <h1>Life in Bangalore</h1>

        <div className="mcq-container">

          <div className="mcq-header"></div>
          
          <div className="mcq-qa">
            <div className="mcq-options"></div>
          </div>

          <div className="mcq-submit"></div>

          <div className="mcq-prog-bar"></div>
        </div>
      </div>
    );
  }
}

export default App;
