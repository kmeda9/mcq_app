import React, { Component, Fragment } from 'react';
import './App.sass';
import Header from './components/Header';
import Body from './components/Body';

class App extends Component {
  render() {
    return (
      <Fragment>
        <div className="wrapper">
          <div className="mcq-container">

            <Header 
              text="Quiz App"
              />
            
            <Body/>

          <div className="mcq-controls">
            
            <div className="mcq-buttons-cont center-align">
              <button className="mcq-btn mcq-next">Next</button>
              <button className="mcq-btn mcq-submit">Submit</button>
              <button className="mcq-btn mcq-results">Results</button>
            </div>
          </div>
            
            <div className="mcq-remaining">1 / 10</div>
            <div className="mcq-prog-bar"></div>

          </div>
        </div>
      </Fragment>
    );
  }
}

export default App;
