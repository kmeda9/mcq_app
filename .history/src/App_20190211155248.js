import React, { Component, Fragment } from 'react';
import './App.sass';
import Header from './components/Header';

class App extends Component {
  render() {
    return (
      <Fragment>
        <div className="wrapper">
          <div className="mcq-container">

            <Header text="Quiz App"/>
            
            <div className="mcq-qa">
              
              <div className="mcq-q">1) What is the best place to live in India ?</div>
              
              <div className="mcq-options">Bangalore</div>
              <div className="mcq-options">Mumbai</div>
              <div className="mcq-options">Hyderabad</div>
              <div className="mcq-options">Chennai</div>
              
            </div>

          <div className="mcq-controls">
            
            <div className="mcq-buttons-cont center-align">
              <button className="mcq-btn mcq-next">Next</button>
              <button className="mcq-btn mcq-submit">Submit</button>
              <button className="mcq-btn mcq-results">Results</button>
            </div>

            
            <div className="mcq-remaining">1 / 10</div>
            <div className="mcq-prog-bar"></div>

          </div>

          </div>
        </div>
      </Fragment>
    );
  }
}

export default App;
