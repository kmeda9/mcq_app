import React, {Component} from 'react';
import {connect} from 'react-redux';

const mapStatetoProps = state => {
  return {
    questions: state.mcq.questions,
    position: state.mcq.position
  }
}

class Body extends Component {

  render() {
    const {questions, position} = this.props;
    
    return (
      <div className="mcq-qa">

      {
        questions.map((question, i) => {
          let {q, options} = question;
          if( i+1 == position ){
            return (
              <div key={i}>
                <div className="mcq-q">
                  {`${i+1}) ${q}`}
                </div>
                <div className="mcq-options">{`a) ${options.a}`}</div>
                <div className="mcq-options">{`b) ${options.b}`}</div>
                <div className="mcq-options">{`c) ${options.c}`}</div>
                <div className="mcq-options">{`d) ${options.d}`}</div>
                
              </div>
            )
          }
        })
      }

      </div>
    )
  }
}

export default connect(mapStatetoProps)(Body);