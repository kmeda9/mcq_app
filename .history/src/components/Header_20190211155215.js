import React from 'react';

export const Header = props => {
  return (
    <div 
      className="mcq-header center-align">
      {props.text}
      </div>
  )
}