import React, {Component} from 'react';

class Progbar extends Component {

  render() {
    return (
      <div className="mcq-prog center-align">
        <div className="mcq-remaining">1 / 10</div>
        <div className="mcq-prog-bar"></div>
      </div>
    )
  }
}

export default Progbar;