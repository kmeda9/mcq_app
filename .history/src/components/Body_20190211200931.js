import React, {Component} from 'react';
import {connect} from 'react-redux';

const mapStatetoProps = state => {
  return {
    questions: state.mcq.questions,
    position: state.mcq.position
  }
}

class Body extends Component {

  constructor(){
    super();
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(e){
    console.log(e.target.value)
    const {dispatch, position} = this.props;
    dispatch({
      type: 'ANSWER_QUESTION',
      position,
      value: e.target.value
    })

  }
  render() {
    const {questions, position} = this.props;
    
    return (
      <div className="mcq-qa">

      {
        questions.map((question, i) => {
          let {options, response} = question;
          if( i+1 == position ){
            return (
              <div key={i}>
                <div className="mcq-q">
                  {`${i+1}) ${question.question}`}
                </div>
                <div className="mcq-options">
                  <input type="radio"
                    value='1'
                    checked={i === response}
                    name="myGroupName" 
                    onChange={this.handleChange}/>
                  <span>{options['1']}</span>
                </div>
                <div className="mcq-options">
                  <input 
                    value='2'
                    checked={i === response}
                    type="radio" name="myGroupName" onChange={this.handleChange}/>
                  <span>{options['2']}</span>
                </div>
                <div className="mcq-options">
                  <input 
                    value='3'
                    checked={i === response}
                    type="radio" name="myGroupName" onChange={this.handleChange}/>
                  <span>{options['3']}</span>
                </div>
                <div className="mcq-options">
                  <input 
                    value='4'
                    checked={i === response}
                    type="radio" name="myGroupName" onChange={this.handleChange}/>
                  <span>{options['4']}</span>
                </div>
                                
              </div>
            )
          }
        })
      }

      </div>
    )
  }
}

export default connect(mapStatetoProps)(Body);