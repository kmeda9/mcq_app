import React, {Component} from 'react';
import {connect} from 'react-redux';

const mapStatetoProps = state => {
  return {
    questions: state.mcq.questions,
    position: state.mcq.position,
  }
}

class Controls extends Component {

  constructor(props){
    super(props);
    this.state = {}

    this.handleIncr = this.handleIncr.bind(this);
    this.handleDecr = this.handleDecr.bind(this);
  }

  handleIncr(){
    const {questions, position, dispatch} = this.props;
    if(position == questions.length) return
    dispatch({type: 'NEXT_QUESTION'})
    
  }
  handleDecr(){
    const {questions, position, dispatch} = this.props;
    if(position == '1') return
    dispatch({type: 'PREV_QUESTION'})
    
  }

  calcProgress(){
    const {questions, dispatch} = this.props;

    let responses = questions.filter(q => q.response);
    return responses ? 
      responses.length/questions.length*100 : 0

  }

  render() {
    let {questions, position} = this.props;
    
    return (
      <div className="mcq-controls center-align">
        <div className="mcq-buttons-cont">
          
          <div className="mcq-prog-bar-cont">
            <div className="mcq-prog-bar"></div>
            <span style={{
              position: 'absolute',
              right: -40,
              top:-6
              }}>{`${this.calcProgress()}%`}</span>
          </div>
          
          <div className="mcq-buttons-nav">
            
              <button 
                className="mcq-btn mcq-btn-sm"
                onClick={this.handleDecr}
                >Prev</button>
              <div 
                className="mcq-remaining center-align"
                style={{marginLeft: 5, marginRight: 5}}
                >{`${position} / ${questions.length}`}</div>
              <button 
                className="mcq-btn mcq-btn-sm"
                onClick={this.handleIncr}
              >Next</button>
            
              <div>
                <button className="mcq-btn mcq-submit">Submit</button>
              </div>
          </div>
            
          {/* <button className="mcq-btn mcq-results">Results</button> */}

        </div>
        
        
      </div>
    )
  }
}

export default connect(mapStatetoProps)(Controls);