import React, {Component} from 'react';
import {connect} from 'react-redux';

const mapStatetoProps = state => {
  return {
    questions: state.mcq.questions,
    position: state.mcq.position
  }
}

class Body extends Component {

  constructor(){
    super();
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(e){
    const {dispatch, position, questions} = this.props;

    
    dispatch({
      type: 'ANSWER_QUESTION',
      position,
      value: e.target.value
    })
    console.log(`Your answer is ${questions[position-1].answer === questions[position-1].response}`)
    this.forceUpdate();

  }
  
  render() {
    const {questions, position} = this.props;
    
    return (
      <div className="mcq-qa">

      {
        questions.map((question, i) => {
          let {options, response} = question;
          if( i+1 == position ){
            
            return (
              <div key={i}>
                <div className="mcq-q">
                  {`${i+1}) ${question.question}`}
                </div>
                <div className="mcq-options">
                  <input type="radio"
                    value='1'
                    checked={'1' == response}
                    name="myGroupName" 
                    onChange={this.handleChange}/>
                  <span>{options['1']}</span>
                </div>
                <div className="mcq-options">
                  <input 
                    value='2'
                    checked={'2' == response}
                    type="radio" name="myGroupName" onChange={this.handleChange}/>
                  <span>{options['2']}</span>
                </div>
                <div className="mcq-options">
                  <input 
                    value='3'
                    checked={'3' == response}
                    type="radio" name="myGroupName" onChange={this.handleChange}/>
                  <span>{options['3']}</span>
                </div>
                <div className="mcq-options">
                  <input 
                    value='4'
                    checked={'4' == response}
                    type="radio" name="myGroupName" onChange={this.handleChange}/>
                  <span>{options['4']}</span>
                </div>
                                
              </div>
            )
          }
        })
      }

      </div>
    )
  }
}

export default connect(mapStatetoProps)(Body);