import React, {Component} from 'react';
import {connect} from 'react-redux';

const mapStatetoProps = state => {
  return {
    questions: state.mcq.questions,
    position: state.mcq.position,
  }
}

class Controls extends Component {

  constructor(props){
    super(props);
    this.state = {}

    this.handleClick = this.handleClick.bind(this);
  }


  render() {
    let {questions} = this.props;

    return (
      <div className="mcq-controls center-align">
        <div className="mcq-buttons-cont">
          
          <div className="mcq-prog-bar-cont">
            <div className="mcq-prog-bar"></div>
            <span style={{
              position: 'absolute',
              right: -40,
              top:-6
              }}>10%</span>
          </div>
          
          <div className="mcq-buttons-nav">
            
              <button 
                className="mcq-btn mcq-btn-sm"
                onClick={this.handleClick}
                >Prev</button>
              <div 
                className="mcq-remaining center-align"
                style={{marginLeft: 5, marginRight: 5}}
                >{`${1} / ${questions.length}`}</div>
              <button 
                className="mcq-btn mcq-btn-sm"
                onClick={this.handleClick}
              >Next</button>
            
              <div>
                <button className="mcq-btn mcq-submit">Submit</button>
              </div>
          </div>
            
          {/* <button className="mcq-btn mcq-results">Results</button> */}

        </div>
        
        
      </div>
    )
  }
}

export default connect(mapStatetoProps)(Controls);