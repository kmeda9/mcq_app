import React, {Component} from 'react';

class Progbar extends Component {

  render() {
    return (
      <div className="mcq-prog">
        <div 
          className="mcq-prog-bar"
          
          ></div>
        <div className="mcq-remaining">1 / 10</div>
      </div>
    )
  }
}

export default Progbar;