import React, {Component} from 'react';

class Body extends Component {

  render() {
    return (
      <div className="mcq-qa">

        <div className="mcq-q">1) What is the best place to live in India ?</div>

        <div className="mcq-options">Bangalore</div>
        <div className="mcq-options">Mumbai</div>
        <div className="mcq-options">Hyderabad</div>
        <div className="mcq-options">Chennai</div>

      </div>
    )
  }
}

export default Body;