import React, {Component} from 'react';

class Controls extends Component {

  render() {
    return (
      <div className="mcq-controls">
        <div className="mcq-prog">
          <div className="mcq-prog-bar"></div>
        </div>
        <div className="mcq-buttons-cont center-align">
          <div style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center'
          }}>
            <button className="mcq-btn mcq-next">Previous</button>
            <div className="mcq-remaining">1 / 10</div>
            <button className="mcq-btn mcq-next">Next</button>
          </div>

          <button className="mcq-btn mcq-submit">Submit</button>
          <button className="mcq-btn mcq-results">Results</button>
        </div>
      </div>
    )
  }
}

export default Controls;