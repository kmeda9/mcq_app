import React, {Component} from 'react';
import Progbar from './Progbar';

class Controls extends Component {

  render() {
    return (
      <div className="mcq-controls">
        <Progbar />
        <div className="mcq-buttons-cont center-align">
          <button className="mcq-btn mcq-next">Previous</button>
          <div className="mcq-remaining">1 / 10</div>
          <button className="mcq-btn mcq-next">Next</button>
          <button className="mcq-btn mcq-submit">Submit</button>
          <button className="mcq-btn mcq-results">Results</button>
        </div>
      </div>
    )
  }
}

export default Controls;