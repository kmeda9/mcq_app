import React, {Component} from 'react';

class Controls extends Component {

  render() {
    return (
      <div className="mcq-controls">
        <div className="mcq-buttons-cont center-align">
          <button className="mcq-btn mcq-next">Previous</button>
          <button className="mcq-btn mcq-next">Next</button>
          {/* <button className="mcq-btn mcq-submit">Submit</button>
          <button className="mcq-btn mcq-results">Results</button> */}
        </div>
      </div>
    )
  }
}

export default Controls;