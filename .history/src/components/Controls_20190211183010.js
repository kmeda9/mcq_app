import React, {Component} from 'react';

class Controls extends Component {

  render() {
    return (
      <div className="mcq-controls center-align">
        <div className="mcq-buttons-cont">
          
          <div className="mcq-prog-bar-cont">
            <div className="mcq-prog-bar-grey">
              <div className="mcq-prog-bar"></div>
              <div>10%</div>
            </div>
          </div>
          
          <div className="mcq-buttons-nav">
            
              <button 
                className="mcq-btn mcq-btn-sm"
                >Prev</button>
              <div 
                className="mcq-remaining center-align"
                style={{marginLeft: 5, marginRight: 5}}
                >1 / 10</div>
              <button className="mcq-btn mcq-btn-sm">Next</button>
            
              <div>
                <button className="mcq-btn mcq-submit">Submit</button>
              </div>
          </div>
            
          {/* <button className="mcq-btn mcq-results">Results</button> */}

        </div>
        
        
      </div>
    )
  }
}

export default Controls;