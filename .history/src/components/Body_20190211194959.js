import React, {Component} from 'react';
import {connect} from 'react-redux';

const mapStatetoProps = state => {
  return {
    questions: state.mcq.questions,
    position: state.mcq.position
  }
}

class Body extends Component {

  constructor(){
    super();
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(){

  }
  render() {
    const {questions, position} = this.props;
    
    return (
      <div className="mcq-qa">

      {
        questions.map((question, i) => {
          let {options} = question;
          if( i+1 == position ){
            return (
              <div key={i}>
                <div className="mcq-q">
                  {`${i+1}) ${question.question}`}
                </div>
                <div className="mcq-options">
                  <input type="radio"
                    value='a'
                    name="myGroupName" 
                    onChange={this.handleChange}/>
                  <span>{`${options.a}`}</span>
                </div>
                <div className="mcq-options">
                  <input 
                    value='b'
                    type="radio" name="myGroupName" onChange={this.handleChange}/>
                  <span>{`${options.b}`}</span>
                </div>
                <div className="mcq-options">
                  <input 
                    value='c'
                    type="radio" name="myGroupName" onChange={this.handleChange}/>
                  <span>{`${options.c}`}</span>
                </div>
                <div className="mcq-options">
                  <input 
                    value='d'
                    type="radio" name="myGroupName" onChange={this.handleChange}/>
                  <span>{`${options.d}`}</span>
                </div>
                                
              </div>
            )
          }
        })
      }

      </div>
    )
  }
}

export default connect(mapStatetoProps)(Body);