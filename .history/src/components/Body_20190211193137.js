import React, {Component} from 'react';
import {connect} from 'react-redux';

const mapStatetoProps = state => {
  return {
    questions: state.mcq.questions,
    position: state.mcq.position
  }
}

class Body extends Component {

  render() {
    const {questions, position} = this.props;
    
    return (
      <div className="mcq-qa">

      {
        questions.map((question, i) => {
        return (
          <div key={i}>
            <div className="mcq-q">
              {`${i+1}) ${question.question}`}
            </div>
            <div className="mcq-options">{`a) ${question.a}`}</div>
            <div className="mcq-options">{`b) ${question.b}`}</div>
            <div className="mcq-options">{`c) ${question.c}`}</div>
            <div className="mcq-options">{`d) ${question.d}`}</div>
            
          </div>
        )
        })
      }

      </div>
    )
  }
}

export default connect(mapStatetoProps)(Body);