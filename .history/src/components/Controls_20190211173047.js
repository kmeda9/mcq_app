import React, {Component} from 'react';

class Controls extends Component {

  render() {
    return (
      <div className="mcq-controls">

        <div style={{
          width: '40%',
          height: '100%',
          margin: 'auto'
          }}>
          
          <div className="mcq-prog-bar"></div>
          
          <div className="mcq-buttons-cont">    
            <div style={{
              width: '30%', 
              height: '100%',
              display: 'flex',
              justifyContent: 'flex-start',
              alignItems: 'center'
              }}>
              <button className="mcq-btn mcq-btn-sm">Prev</button>
              <div className="mcq-remaining">1 / 10</div>
              <button className="mcq-btn mcq-btn-sm">Next</button>
            </div>
            
            <div>
              <button className="mcq-btn mcq-submit">Submit</button>
              <button className="mcq-btn mcq-results">Results</button>
            </div>
          </div>

        </div>
        
        
      </div>
    )
  }
}

export default Controls;