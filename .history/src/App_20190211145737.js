import React, { Component, Fragment } from 'react';
import './App.sass';

class App extends Component {
  render() {
    return (
      <Fragment>
        <div className="wrapper">
          <div className="mcq-container">

            <div className="mcq-header">Quiz App</div>
            
            <div className="mcq-qa">
              <div className="mcq-options"></div>
            </div>

          <div className="mcq-controls">
            <div className="mcq-next"></div>
            
            <div className="mcq-submit"></div>
            <div className="mcq-results"></div>

            <div className="mcq-prog-bar"></div>

          </div>

          </div>
        </div>
      </Fragment>
    );
  }
}

export default App;
