import React, { Component, Fragment } from 'react';
import './App.sass';

class App extends Component {
  render() {
    return (
      <Fragment>
        <div className="wrapper">
          <div className="mcq-container">

            <div className="mcq-header center-align">Quiz App</div>
            
            <div className="mcq-qa">
              <div className="mcq-q">1) What is the best place to live in India ?</div>
              <div className="mcq-options">Bangalore</div>
              <div className="mcq-options">Mumbai</div>
              <div className="mcq-options">Hyderabad</div>
              <div className="mcq-options">Chennai</div>
            </div>

          <div className="mcq-controls">
            <div className="mcq-next">Next</div>
            
            <div className="mcq-submit">Submit</div>
            <div className="mcq-results">Results</div>

            <div className="mcq-prog-bar"></div>

          </div>

          </div>
        </div>
      </Fragment>
    );
  }
}

export default App;
