import React, {Component, Fragment} from 'react';
import './App.sass';
import Header from './components/Header';
import Body from './components/Body';
import Controls from './components/Controls';

import {connect} from 'react-redux';
import mcqReducer from './reducers/mcqReducer';

const mapStateToProps = state => {
  return {
    submitted: state.mcq.submitted,
    questions: state.mcq.questions
  }
}

class App extends Component {
  constructor(){
    super();
    this.state={displayResults: false }
    this.showResults = this.showResults.bind(this);
  }
  
  showResults(){
    this.setState(prevState => {
      return {
        displayResults: !prevState.displayResults
      }
    });
    
  }

  renderResults(){
    const {questions} = this.props;
    return (
      <div style={{padding: 30}}>
        {
          questions.map((question, i) => {
            return (
              <div key = {i} style={{padding: 20}}>
                <i>{`${i+1})`}</i>
                <span style={{marginLeft: 5}}>{question.question}</span>
                <div style={{marginLeft: 20}}>Answer: {question.answer} </div>
                <div style={{marginLeft: 20}}>Given: {question.response} </div>
              </div>
            )
        })}
        <button
        className="mcq-btn"
        >
        Restart</button>
      </div>
    )
  }
  
  render() {
    return (
      <Fragment>
        <div className="wrapper">
          <div className="mcq-container">

            <Header text="Quiz App"/> 
            {!this.props.submitted
              ? <Fragment>
                  <Body/>
                  <Controls/>
                </Fragment>
              : 
              this.state.displayResults ? this.renderResults() :
              <Fragment>
                <div style={{
                  width: '100%',
                  height: '80%',
                  padding: '20px',
                  display: 'flex',
                  justifyContent: 'center',
                  alignItems: 'center',
                  flexDirection: 'column',
                }}> 
                  <div>Your's answers have been submitted!</div>
                  <div>Click on Results to verify!</div>
                <div style={{marginTop: 20}}>
                  <button 
                    className="mcq-btn mcq-results"
                    onClick={this.showResults}
                    >Results</button>
                </div>
                </div>
              </Fragment>
}

          </div>
        </div>
      </Fragment>
    );
  }
}

export default connect(mapStateToProps)(App);
