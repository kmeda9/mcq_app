import React, { Component, Fragment } from 'react';
import './App';
class App extends Component {
  render() {
    return (
      <Fragment>

        <div className="mcq-container">

          <div className="mcq-header"></div>
          
          <div className="mcq-qa">
            <div className="mcq-options"></div>
          </div>

          <div className="mcq-next"></div>
          
          <div className="mcq-submit"></div>
          <div className="mcq-results"></div>

          <div className="mcq-prog-bar"></div>

        </div>
      </Fragment>
    );
  }
}

export default App;
