import React, {Component, Fragment} from 'react';
import './App.sass';
import Header from './components/Header';
import Body from './components/Body';
import Controls from './components/Controls';

import {connect} from 'react-redux';
import mcqReducer from './reducers/mcqReducer';

const mapStateToProps = state => {
  return {
    submitted: state.mcq.submitted}
}

class App extends Component {
  constructor(){
    super()
    this.showResults = this.showResults.bind(this);
  }
  showResults(){

  }
  render() {
    return (
      <Fragment>
        <div className="wrapper">
          <div className="mcq-container">

            <Header text="Quiz App"/> 
            {!this.props.submitted
              ? <Fragment>
                  <Body/>
                  <Controls/>
                </Fragment>
              : 
              <Fragment>
                <div>
                  <div>Your's answers have been submitted!</div>
                  <div>Click on Results to verify!</div>
                </div>
                <div>
                  <button 
                    className="mcq-btn mcq-results"
                    onClick={this.showResults}
                    >Results</button>
                </div>
              </Fragment>
}

          </div>
        </div>
      </Fragment>
    );
  }
}

export default connect(mapStateToProps)(App);
