import React, { Component, Fragment } from 'react';
import './App.sass';

class App extends Component {
  render() {
    return (
      <Fragment>
        <div className="wrapper">
          <div className="mcq-container">

            <div className="mcq-header center-align">Quiz App</div>
            
            <div className="mcq-qa">
              <div className="mcq-q">What is the best place to live in India ?</div>
              <div className="mcq-options">Bangalore</div>
              <div className="mcq-options">Mumbai</div>
              <div className="mcq-options">Hyderabad</div>
              <div className="mcq-options">Chennai</div>
            </div>

          <div className="mcq-controls">
            <div className="mcq-next"></div>
            
            <div className="mcq-submit"></div>
            <div className="mcq-results"></div>

            <div className="mcq-prog-bar"></div>

          </div>

          </div>
        </div>
      </Fragment>
    );
  }
}

export default App;
